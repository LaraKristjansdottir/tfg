#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Lára
"""

import logging
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from   sklearn.manifold import TSNE

#Project modules
import config_manager
import file_manager
import common
import synthetic_data
import merge_and_assign_class
import similarity_matrix

def outlier_detection(n_samples, n_jobs, n_estimators):

    if n_samples < 119:
        raise Exception('El número de muestras no puede ser menor que 119')
    
    logging.basicConfig(filename='log/outlier_detection_log.txt', level=logging.INFO, format='%(asctime)s %(levelname)s:%(message)s')
    
    # Extract data for outlier detection
    logging.info('---------------------------------------------------------')
    config = config_manager.generate_config()
    logging.info('A new execution has begun, working with %d samples and %d jobs', n_samples, n_jobs)
    
    (X, ids) = common.load_feature_matrix_random(config['fits_directory'], n_samples)
    logging.info('Feature matrix has been loaded')
    
    # Generating synthetic data based in the marginal distribution
    X_syn = synthetic_data.get_synthetic_data(X)
    # Merge X and X_syn into one sample with their assigned classes
    X_total, Y_total = merge_and_assign_class.merge_data_and_synthetic_samples(X, X_syn)
    logging.info('Synthetic data generated')
    
    # Random forrest training (RF)
    rand_f = RandomForestClassifier(n_estimators=n_estimators, n_jobs = n_jobs)
    rand_f.fit(X_total, Y_total)
    logging.info('Random Forest model was fitted')
    
    # Building similarity matrix and building distances matrix
    sim_mat = similarity_matrix.build_similarity_matrix(rand_f, X)
    dis_mat = 1 - sim_mat
    # Sum matrix to get outlier score (wierdness score)
    sum_vec = np.sum(dis_mat, axis=1)
    sum_vec /= float(len(sum_vec))
    logging.info('Similarity matrix and weirdness score computed')
    
    
    #Dimentional reduction with t-Sne
    tsne = TSNE(n_components=2, verbose=1, learning_rate=1000, perplexity=2000)
    tsne_results = tsne.fit_transform(dis_mat)
    logging.info('TSN2 computed')
    
    df_tsne = pd.DataFrame(tsne_results, index=ids)
    df_tsne['W_S'] = pd.Series(sum_vec, index=ids)
    file_manager.save_results(df_tsne, n_samples, 'tsne_estimators_' + str(n_estimators) + '_')
    
    logging.info('The program has finished')
