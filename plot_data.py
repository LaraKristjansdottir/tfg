# -*- coding: utf-8 -*-
"""
@author: Lára
"""

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import plot_tsne

df_tsne = pd.read_csv('input/output_20000_samples_20180902_015513.csv', sep=';')
plot_tsne.do_plot(df_tsne.loc[:,'0'].values, df_tsne.loc[:,'1'].values, df_tsne.loc[:,'W_S'].values, 20000)

#plot_tsne.do_plot(tsne1_results[:,0], tsne1_results[:,1], sum_vec, 4000)

#dataframe for inner join with paper data
df = pd.DataFrame(tsne1_results, index=ids)
allstar_fits_filename = config['allstar_fits_filename']
with fits.open(allstar_fits_filename) as hdulist:

    allstar = pd.DataFrame([hdulist[1].data['APOGEE_ID'],
                        hdulist[1].data['RA'],
                        hdulist[1].data['DEC'],
                        hdulist[1].data['TEFF'],
                        hdulist[1].data['LOGG'],
                        hdulist[1].data['O_FE'],
                        hdulist[1].data['C_FE'],
                        hdulist[1].data['M_H']], 
                        index = ['APOGEE_ID', 
                        'RA', 
                        'DEC',
                        'TEFF',
                        'LOGG',
                        'O_FE',
                        'C_FE',
                        'M_H'])

allstar = allstar.T
allstar = allstar.set_index('APOGEE_ID')
allstar = allstar[~allstar.index.duplicated(keep='first')]

allstar_w_tsne = allstar.merge(df, left_index=True, right_index=True, how = 'inner')
#add wierdness score column to dataframe
allstar_w_tsne = allstar_w_tsne.insert(loc=0, column='W_S', value=sum_vec)

#Plot t-Sne
#TODO: Weird objects in the front and larger.
plot_tsne.do_plot(allstar_w_tsne[0].tolist(), allstar_w_tsne[1].tolist(), allstar_w_tsne['W_S'])
plot_tsne.do_plot(allstar_w_tsne[0].tolist(), allstar_w_tsne[1].tolist(), allstar_w_tsne['TEFF'])
plot_tsne.do_plot(allstar_w_tsne[0].tolist(), allstar_w_tsne[1].tolist(), allstar_w_tsne['RA'])
plot_tsne.do_plot(allstar_w_tsne[0].tolist(), allstar_w_tsne[1].tolist(), allstar_w_tsne['DEC'])
plot_tsne.do_plot(allstar_w_tsne[0].tolist(), allstar_w_tsne[1].tolist(), allstar_w_tsne['LOGG'])
plot_tsne.do_plot(allstar_w_tsne[0].tolist(), allstar_w_tsne[1].tolist(), allstar_w_tsne['O_FE'])
plot_tsne.do_plot(allstar_w_tsne[0].tolist(), allstar_w_tsne[1].tolist(), allstar_w_tsne['C_FE'])
plot_tsne.do_plot(allstar_w_tsne[0].tolist(), allstar_w_tsne[1].tolist(), allstar_w_tsne['M_H'])

tsne_x = allstar_w_tsne[0].values.astype(float)
tsne_y = allstar_w_tsne[1].values.astype(float)

n_samples = allstar_w_tsne.shape[0]

plot_tsne.do_plot(tsne_x, tsne_y, allstar_w_tsne['TEFF'].values.astype(float), n_samples)

plot_tsne.do_plot(tsne_x, tsne_y, sum_vec, n_samples)


