# -*- coding: utf-8 -*-
"""
@author: Lara
"""

#Realizamos una auditoría de los ficheros fits, para determinar si alguno de ellos está corrupto.

import os
from astropy.io import fits

data_dir = 'F:\Data TFG\APOGEE_spectra'
ok_files = []
corrupt_files = []

count = 1
total = len(os.listdir(data_dir))
for file_name in os.listdir(data_dir):
    print(file_name, count, ' of ', total)
    try:
        x = fits.getdata(os.path.join(data_dir, file_name), 1)
        ok_files.append(file_name)
    except:
        corrupt_files.append(file_name)
    count += 1
