# -*- coding: utf-8 -*-
"""
@author: Lara
"""

import numpy as np
import pandas as pd
from astropy.io import fits

input_file = '../input/output_20000_samples_20180902_015513.csv'

#Leemos el DataFrame generado por TSNE
df = pd.read_csv(input_file, sep=';', index_col=0)

#dataframe for inner join with paper data
allstar_fits_filename = '../papers/fits/allStar-l31c.2.fits'
with fits.open(allstar_fits_filename) as hdulist:

    allstar = pd.DataFrame([hdulist[1].data['APOGEE_ID'],
                        hdulist[1].data['RA'],
                        hdulist[1].data['DEC'],
                        hdulist[1].data['TEFF'],
                        hdulist[1].data['LOGG'],
                        hdulist[1].data['O_FE'],
                        hdulist[1].data['C_FE'],
                        hdulist[1].data['M_H']], 
                        index = ['APOGEE_ID', 
                        'RA', 
                        'DEC',
                        'TEFF',
                        'LOGG',
                        'O_FE',
                        'C_FE',
                        'M_H'])

allstar = allstar.T
allstar = allstar.set_index('APOGEE_ID')
allstar = allstar[~allstar.index.duplicated(keep='first')]

allstar_w_tsne = allstar.merge(df, left_index=True, right_index=True, how = 'inner')
#add wierdness score column to dataframe

allstar_w_tsne.to_csv('allstar.csv', sep=';')
