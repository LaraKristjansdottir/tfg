# -*- coding: utf-8 -*-
"""
@author: Lára
"""

import pandas as pd
import os

def get_corrupt_files():
    with open('../other_data/corrupt_fits.txt') as f:
        corrupt_files = f.read().splitlines()
        return corrupt_files
    
my_data = pd.read_csv('../other_data/especiales.csv', sep=';')
corrupt_files = get_corrupt_files()

for id in my_data.loc[:,'ID'].values:
    file_name = 'aspcapStar-r8-l31c.1-' + id + '.fits'
    path = os.path.join('F:/Data TFG/APOGEE_spectra', file_name)
    if not os.path.exists(path):
        print(id)
    if file_name in corrupt_files:
        print(id)
    
