# -*- coding: utf-8 -*-
"""
@author: Lára
"""

import numpy as np
import pandas as pd

input_file = '../input/tsne_20000_samples_20180903_093428.csv'
targets_file = '../other_data/targets.csv'

#Leemos el DataFrame generado por TSNE
df = pd.read_csv(input_file, sep=';', index_col=0)

#Leemos el DataFrame con las estrellas de especial interés. Eliminamos las que no existen
targets_df = pd.read_csv(targets_file, sep=';', index_col=0)
targets_df = targets_df.loc[targets_df['exists'] == True]

#Obtenemos las 10 estrellas con mayor outlier score, junto con sus identificadores.
#Exportamos como tabla de LaTeX
ws_series_sorted = df['W_S'].sort_values(ascending=False)
most_outlier_series = ws_series_sorted.head(10)
#print(most_outlier_series.to_latex())

#Componemos el outlier_score de la estrellas de especial interés
df_sorted = df.sort_values('W_S', ascending=False)
df_sorted['ranking'] = pd.Series(np.array(np.arange(1, df.shape[0]+1)), index=df_sorted.index)
df_join = df_sorted.join(targets_df, how='inner')
df_join = df_join.drop_duplicates()
df_join_selection = df_join[['W_S', 'ranking', 'origin']].sort_values('W_S', ascending=False)
#print(df_join_selection.to_latex())

#Añadimos al df original una columna indicando si la muestra es de especial interés. Mantenemos el origen.
#Guardamos a disco.
df_for_hist = df.join(targets_df, how='left')
df_for_hist['target'] = df_for_hist['exists'].values == True
df_for_hist.drop('exists', inplace=True, axis=1)
df_for_hist.to_csv('hist_data.csv', sep=';')