# -*- coding: utf-8 -*-
"""
@author: Lára
"""

import os
import time
#import pandas as pd

def save_results(df, n_samples, prefix):
    timestr = time.strftime("%Y%m%d_%H%M%S")
    output_dir = 'output'
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    df.to_csv(os.path.join(output_dir, prefix + '_' + str(n_samples) + '_samples_' + timestr + '.csv'), sep=';')
