#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Lára
"""

import matplotlib.pyplot as plt
import numpy as np

def do_plot(tsne_x, tsne_y, characteristics_vector, number_of_samples):
    sample = np.arange(number_of_samples)    
    sample_sort = sample[np.argsort(characteristics_vector[sample])] # Weird objects in the front
    
    cmin = characteristics_vector.min()
    cmax = characteristics_vector.max()
    s = (characteristics_vector - cmin)/(cmax - cmin) * 20 + (cmax - characteristics_vector)/(cmax - cmin) * 0.1
          
    plt.scatter(tsne_x[sample_sort], tsne_y[sample_sort], c=characteristics_vector[sample_sort], s=s, vmin=cmin, vmax=cmax, cmap='plasma_r') 
    plt.title('characteristics_vector')
    plt.axis('off')
    plt.colorbar()
    plt.tight_layout()
    plt.show()
