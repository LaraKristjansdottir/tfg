# -*- coding: utf-8 -*-
"""
@author: Lara
"""

import json

def generate_config():
    with open('config/config.json') as f:
        return json.load(f)
