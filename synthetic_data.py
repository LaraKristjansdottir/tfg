# -*- coding: utf-8 -*-
"""
Creating synthetic data the same size as data for outlier detection. 
This is done for training the RF algorithm on a balanced sample.

@author: Lara
"""

import numpy as np
#function return a matrix with same dimensions as input with synthetic data with the same marginal distributions as the data for outlier detection
def get_synthetic_data(X):
    features = X.shape[1]
    X_syn = np.zeros(X.shape) #Zerosmatrix same shape MXN as input matrix

    for i in range(features):
        obs_vec = X[:,i]
        syn_vec = np.random.choice(obs_vec, len(obs_vec)) #Choosing data to match the marginal distribution of the real data
        X_syn[:,i] += syn_vec 

    return X_syn
