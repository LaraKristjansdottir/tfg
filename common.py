# -*- coding: utf-8 -*-
"""
@author: Lara
"""

import os
import random
import numpy as np
from astropy.io import fits

def load_feature_matrix_from_dir(data_dir):
    n_features = 8575
    n_rows = len(os.listdir(data_dir))
    feature_matrix = np.empty(shape=(n_rows, n_features))
    
    for i, file_name in enumerate(os.listdir(data_dir)):
        feature_matrix[i,:] = fits.getdata(os.path.join(data_dir, file_name), 1)
        
    return feature_matrix

def load_feature_matrix_random(data_dir, n_samples):
    n_features = 8575
    n_rows = n_samples
    feature_matrix = np.empty(shape=(n_rows, n_features))
    
    target_files = get_target_files()
    sample_ids = []
    
    #Primero cargamos los targets
    count_targets = 0
    for i, file_name in enumerate(target_files):
        feature_matrix[i,:] = fits.getdata(os.path.join(data_dir, file_name), 1)
        sample_ids.append(file_name.replace('aspcapStar-r8-l31c.1-', '').replace('.fits', ''))
        count_targets +=1
    
    #Rellenamos con ficheros aleatorios no corruptos
    corrupt_files = get_corrupt_files()
    file_names = [x for x in os.listdir(data_dir) if x not in corrupt_files]
    for i, file_name in enumerate(random.sample(file_names, n_samples-count_targets)):
        feature_matrix[i+count_targets,:] = fits.getdata(os.path.join(data_dir, file_name), 1)
        sample_ids.append(file_name.replace('aspcapStar-r8-l31c.1-', '').replace('.fits', ''))
        
    return (feature_matrix, sample_ids)

def get_corrupt_files():
    with open('other_data/corrupt_fits.txt') as f:
        corrupt_files = f.read().splitlines()
        return corrupt_files

def get_target_files():
    with open('other_data/targets.txt') as f:
        corrupt_files = f.read().splitlines()
        return corrupt_files
    