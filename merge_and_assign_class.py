# -*- coding: utf-8 -*-
"""
Label for 'data for outlier detection' as Class I.
Label 'synthetic' data as Class II.
Merge the two samples with their labels into a data matrix X and label vector Y.
@author: Lara
"""
import numpy as np

#Merge the two data matrices with their labels into a data matrix X and label vector Y
def merge_data_and_synthetic_samples(X, X_syn):

    # build the labels vector
    Y = np.ones(len(X))
    Y_syn = np.ones(len(X_syn)) * 2

    Y_total = np.concatenate((Y, Y_syn))
    X_total = np.concatenate((X, X_syn))
    
    return X_total, Y_total
