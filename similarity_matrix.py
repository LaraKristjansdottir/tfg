# -*- coding: utf-8 -*-
"""
    builds the similarity matrix based on the feature matrix X for the results Y
    based on already trained random forest.
@author: Lara
"""
import numpy as np
import logging

def build_similarity_matrix(rand_f, X):
    # apply to get the leaf indices
    apply_mat = rand_f.apply(X)
    # find the predictions of the sample
    is_good_matrix = np.zeros(apply_mat.shape)
    
    for i, est in enumerate(rand_f.estimators_):
        d = est.predict_proba(X)[:, 0] == 1
        is_good_matrix[:, i] = d
    
    # mark leaves that make the wrong prediction as -1, in order to remove them from the distance measurement
    apply_mat[is_good_matrix == False] = -1 
    
    # now calculate the similarity matrix
    numerator = compute_numerator_numpy(apply_mat)
    denominator = np.asfarray(np.sum([apply_mat != -1], axis=2), dtype='float')
    sim_mat = numerator / denominator
    return sim_mat

def compute_numerator(apply_mat):
    n_samples = apply_mat.shape[0]
    numerator = np.zeros([n_samples, n_samples])
    for i in range(n_samples):
        for j in range(i+1):
            numerator[i,j] = count_same_prediction(apply_mat[i], apply_mat[j])
            numerator[j,i] = numerator[i,j]            
    return numerator    

def count_same_prediction(row_a, row_b):
    return np.sum((row_a != -1) & (row_b != -1) & (row_a == row_b))

def compute_numerator_numpy(apply_mat):
    n_samples = apply_mat.shape[0]
    numerator = np.zeros([n_samples, n_samples])
    exclusions = np.ones(apply_mat.shape[1]) * -1
    for i in range(n_samples):
        numerator[i] = np.sum((apply_mat == apply_mat[i]) & (apply_mat != exclusions), axis=1)
        if i % 100 == 0:
            logging.info('Row ' + str(i) + ' computed')
    return numerator